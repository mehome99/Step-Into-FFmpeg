#Step Into FFmpeg

# About subdirectory
## vs2013_build
my ffmpeg(v2.8.2) dev-output by vs2013+mingw

## VideoScaleTest
libswscale demo, produce a 320x240 yuv420 image and convert to rgb & user-defined size.

## AudioResampleTest
libresample demo, produce 10s audio, change audio layout, sample_rate and sample_format.


